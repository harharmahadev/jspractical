/*
4. All the ways to copy an array. Utilize Shallow and Deep copy.

*/

var person = {
	id: "01",
	name: "Hardik",
	salary: 10000000
}


console.log("Person data=> ", person);
var newPerson = person;  // Shallow copy
console.log("New Person=> ", newPerson);

newPerson.name = "Ahir";
console.log("Deep Person data=> ", person);
console.log("New Person=> ", newPerson);

