/*
1. Given a string containing just the characters '(' and ')', find the whether the expression is valid or not.

*/

function isValid(exp){
 
    let flag = true
    let count = 0
     for(let i=0;i<exp.length;i++){
        if (exp[i] == '(')
            count += 1
        else
            count -= 1
 
        if (count < 0){           
            flag = false
            break
        }
    }
   
    if (count != 0)
        flag = false
 
    return flag
}
      
let exp1 = "((()))()()"
 
if (isValid(exp1))
    console.log("Valid")
else
    console.log("Not Valid")
 
let exp2 = ")()"
 
if (isValid(exp2))
    console.log("Valid")
else
    console.log("Not Valid")